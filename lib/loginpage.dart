
import 'package:flutter/material.dart';

import 'mainpage.dart';


class LoginPage extends StatelessWidget {
  const LoginPage({super.key});

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;

    return Scaffold(
      backgroundColor: Colors.black,
      body: GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(),
        child: SizedBox(
          height: height,
          child: Stack(
            children: [
              Container(
                height: height / 2,
                width: double.infinity,
                decoration: BoxDecoration(
                    color: Colors.white10,
                    borderRadius: const BorderRadius.only(
                      bottomRight: Radius.circular(60),
                      bottomLeft: Radius.circular(60),
                    )
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(
                        height: 70,
                        child: Image.asset('assets/playlist15.png')),
                    const SizedBox(height: 20,),
                    const Text('MIllions of songs, free on this Musical app', style: TextStyle(fontSize: 18, color: Colors.white, fontWeight: FontWeight.w600, fontFamily: 'Roboto', fontStyle: FontStyle.normal),)
                  ],
                ),
              ),
              SingleChildScrollView(
                child: SizedBox(
                  height: height,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Container(
                        padding: const EdgeInsets.symmetric(horizontal: 32, vertical: 20),
                        height: height / 1.9,
                        margin: const EdgeInsets.all(20),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(32)
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            const Text('Login Account', style: TextStyle(fontSize: 23, fontWeight: FontWeight.w500), textAlign: TextAlign.center,),
                            const SizedBox(height: 22,),
                            SizedBox(
                              height: 40,
                              child: Input(hint: 'Email or Username', icon: Icons.email_outlined),
                            ),
                            const SizedBox( height:16),
                            SizedBox(
                              height: 40,
                              child: Input(hint: 'Password', icon: Icons.visibility_outlined),
                            ),
                            SwitchListTile.adaptive(
                              value: true,
                              onChanged: ((value) {}),
                              contentPadding: const EdgeInsets.all(0),
                              title: Text('Remember me', style: TextStyle(fontSize: 14, fontWeight: FontWeight.w400, color: Colors.white)),
                            ),
                            MaterialButton(
                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(31)),
                              height: 40,
                              color: Colors.white10,
                              onPressed: () => Navigator.push(context , MaterialPageRoute(builder: (context) =>  HomePage())),
                              child: const Text('LOG IN', style: TextStyle(color: Colors.green, fontSize: 13, fontWeight: FontWeight.w700),) ,
                            ),
                            const SizedBox(height: 10,),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: const [
                                Expanded(
                                  child: Divider(
                                    thickness: 1,
                                    height: 1,
                                    color: Colors.green,
                                  ),
                                ),
                                SizedBox(width: 12,),
                                Text('or', style: TextStyle(color: Colors.white10, fontSize: 13, fontWeight: FontWeight.w700)),
                                SizedBox(width: 12,),
                                Expanded(
                                  child: Divider(
                                    thickness: 1,
                                    height: 1,
                                    color: Colors.white10,
                                  ),
                                ),
                              ],
                            ),
                            const SizedBox(height: 16,),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                SizedBox(
                                    height: 40,
                                    child: Image.asset('assets/google+.png')),
                                const SizedBox(width: 16,),
                                SizedBox(
                                    height: 40,
                                    child: Image.asset('assets/facebook.png')),
                              ],
                            ),
                            const SizedBox(height: 16,),
                            Text('Forget password?',
                              style: TextStyle(fontSize: 14, fontWeight: FontWeight.w400, color: Colors.green),
                              textAlign: TextAlign.center,),
                          ],
                        ),
                      ),
                      const SizedBox(height: 16,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Text('Don’t have an account?', style: TextStyle(color: Colors.white, fontSize: 14, fontWeight: FontWeight.w500),),
                          const SizedBox(width: 20,),
                          Text('Sign up now', style: TextStyle(color: Colors.white, fontWeight: FontWeight.w700),)
                        ],
                      ),
                      const SizedBox(height: 44,)
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

// ignore: must_be_immutable
class Input extends StatelessWidget {
  Input({
    Key? key, required this.hint, required this.icon,
  }) : super(key: key);

  String hint;
  IconData icon;

  @override
  Widget build(BuildContext context) {
    return TextField(
      decoration: InputDecoration(
          labelText: hint,
          labelStyle: const TextStyle(
            fontSize: 12,
            fontWeight: FontWeight.w400,
          ),
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(26),
              borderSide: BorderSide(color: Colors.white10)
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.white10),
            borderRadius: const BorderRadius.all(Radius.circular(26)),
          ),
          suffixIcon: Icon(icon)
      ),
    );
  }
}
